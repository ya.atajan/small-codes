import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class lcsTest {

    // Where x & y length = 0
    @Test
    void lcs_x_y_empty() {
        // Given
        String x = "";
        String y = "";
        //Then
        Assertions.assertEquals("", lcs.lcs(x, y));
    }

    // Where x & y are same length
    @Test
    void lcs_x_y_same() {
        // Given
        String x = "GGCACCACG";
        String y = "GGCACCACG";
        //Then
        Assertions.assertEquals("GGCACCACG", lcs.lcs(x, y));
    }

    // Where x & y are different length and y is longer
    @Test
    void lcs_x_y_different_y() {
        // Given
        String x = "GGCACCACG";
        String y = "ACGGCGGATACG";
        //Then
        Assertions.assertEquals("GGCAACG", lcs.lcs(x, y));
    }

    // Where x & y are different length and x is longer
    @Test
    void lcs_x_y_different_x() {
        // Given
        String x = "ACGGCGGATACG";
        String y = "GGCACCACG";
        //Then
        Assertions.assertEquals("GGCAACG", lcs.lcs(x, y));
    }
}