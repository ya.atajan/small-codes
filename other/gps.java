import java.util.concurrent.ThreadLocalRandom;

public class gps {
    private static int[] coordinate = {0, 0};
    private static int hour = 12;

    private static int getCoordinates() {
        int choice = ThreadLocalRandom.current().nextInt(-1, 2);

        if (choice == 0) {
            getCoordinates();
        }
        return choice;
    }

    private static double getDist(int x, int y) {
        return Math.sqrt(Math.pow(Math.abs(x), 2) + Math.pow(Math.abs(y), 2));
    }

    public static void main(String[] args) {
        while (hour != 0) {
            if (ThreadLocalRandom.current().nextInt(0, 2) > 0) {
                coordinate[0] += getCoordinates();
            } else {
                coordinate[1] += getCoordinates();
            }
            hour--;
        }

        System.out.println("Distance is " + getDist(coordinate[0], coordinate[1]));
    }
}