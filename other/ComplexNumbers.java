import java.util.Scanner;

public class ComplexNumbers {
    static float a1, b1, a2, b2;

    public static void main(String[] args) {


        Scanner keyboard = new Scanner(System.in);

        System.out.println("z1 = a1 + (i*b1) and z2 = a2 + (i*b2)");
        System.out.println("Please enter four numbers");

        System.out.print("Enter a1 :");
        a1 = keyboard.nextFloat();

        System.out.print("Enter b1 :");
        b1 = keyboard.nextFloat();

        System.out.print("Enter a2 :");
        a2 = keyboard.nextFloat();

        System.out.print("Enter b2 :");
        b2 = keyboard.nextFloat();

        addition();
        subtraction();
        multiplication();
        division();
    }

    private static void addition() {
        double add = (a1 + a2);

        if ((b1 + b2) != 0) {
            System.out.println("addtion" + ": z1 + z2 =" + add + " + (" + ((b1 + b2)) + ")i");
        } else {
            System.out.println("addtion" + ": z1 + z2 =" + add);
        }
    }

    private static void subtraction() {
        double sub = (a1 - a2);
        if ((b1 - b2) != 0) {
            System.out.println("subtraction" + ": z1 - z2 =" + sub + " + (" + ((b1 - b2)) + ")i");
        } else {
            System.out.println("subtraction" + ": z1 - z2 =" + sub);
        }
    }

    private static void multiplication() {
        double mul = (a1 * a2 - b1 * b2);
        if ((a1 * b2 + b1 * a2) != 0) {
            System.out.println("multiplication" + ": z1 * z2 =" + mul + " + (" + ((a1 * b2 + b1 * a2)) + ")i");
        } else {
            System.out.println("multiplication" + ": z1 * z2 =" + mul);
        }
    }

    private static void division() {
        double div = (a1 * a2 + b1 * b2) / (Math.pow(a2, 2) + Math.pow(b2, 2));
        if (((b1 * a2 - a1 * b2) / (Math.pow(a2, 2) + Math.pow(b2, 2)) != 0)) {
            System.out.println("division" + ": z1 * z2 =" + div + " + (" + (((b1 * a2 - a1 * b2) / (Math.pow(a2, 2) + Math.pow(b2, 2))) + ")i"));
        } else {
            System.out.println("division" + ": z1 * z2 =" + div);
        }
    }
}
