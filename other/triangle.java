import java.util.Scanner;

public class triangle {
    private static double[][] points = new double[3][2];

    private static double getArea() {
        double p1 = points[0][0] + points [0][1];
        double p2 = points[1][0] + points [1][1];
        double p3 = points[2][0] + points [2][1];

        if (p1 - p2 - p3 == 0) {
            return 0;
        }

        double s1 = Math.hypot(points[0][0]-points[1][0], points [0][1]-points [1][1]);
        double s2 = Math.hypot(points[1][0]-points[2][0], points [1][1]-points [2][1]);
        double s3 = Math.hypot(points[2][0]-points[0][0], points [2][1]-points [0][1]);
        double s = ((s1 + s2 + s3)/2);

        return Math.sqrt(s * (s - s1) * (s - s2) * (s - s3));
    }

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 2; j++) {
                String prompt = String.format("Enter point for [%d][%d]:", i, j);
                System.out.print(prompt);
                points[i][j] = keyboard.nextDouble();
            }
        }

        System.out.println(getArea());
    }
}
