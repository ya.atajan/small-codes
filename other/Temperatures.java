import java.util.*;

public class Temperatures {
    private static int[] highArray = new int[12];
    private static int[] lowArray = new int[12];
    private static Set<Integer> months = new HashSet<>();

    private static int findHighestTemp(int[] array) {
        int maxValue = array[0];
        int index = 0;

        for (int i = 1; i < array.length; i++) {
            if (array[i] > maxValue) {
                index = i;
            }
        }
        return index;
    }

    private static int findLowestTemp(int[] array) {
        int minValue = array[0];
        int index = 0;

        for (int i = 1; i < array.length; i++) {
            if (array[i] < minValue) {
                index = i;
            }
        }
        return index;
    }

    private static float calculateAverage(int[] specArr) {
        int total = 0;

        for (int i = 0; i < 12; i++) {
            total += specArr[i];
        }
        return (float) (total)/12;
    }

    private static float calculateAverageHigh() {
        return calculateAverage(highArray);
    }

    private static float calculateAverageLow() {
        return calculateAverage(lowArray);
    }

    private static void inputTempForMonth(int index, int high, int low) {
        highArray[index - 1] = high;
        lowArray[index - 1] = low;
    }

    private static int[][] inputTempForYear() {
        int[][] year = new int[12][2];
        for (int i = 0; i < 12; i++) {
            year[i][0] = highArray[i];
            year[i][1] = lowArray[i];
        }
        return  year;
    }

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);

        while (months.size() != 12) {
            int month;
            int high;
            int low;

            System.out.println("Enter Month");
            month = keyboard.nextInt();

            if (!months.contains(month)) {
                months.add(month);
                System.out.println("Enter high temp");
                high = keyboard.nextInt();
                System.out.println("Enter low temp");
                low = keyboard.nextInt();

                inputTempForMonth(month, high, low);
            } else {
                System.out.println("The month already exists, enter new month");
            }
        }

        for (int i = 0; i < 12; i++) {
            System.out.println("For month " + i);
            System.out.println("High is " + inputTempForYear()[i][0]);
            System.out.println("Low is " + inputTempForYear()[i][1]);
        }
        System.out.println("Average high is " + calculateAverageHigh());
        System.out.println("Average low is " + calculateAverageLow());
        System.out.println("Max temp was in the month of " + findHighestTemp(highArray) + 1 + "with temperature of " + inputTempForYear()[findHighestTemp(highArray)][0]);
        System.out.println("Min temp was in the month of " + findLowestTemp(lowArray) + 1 + "with temperature of " + inputTempForYear()[findLowestTemp(lowArray)][1]);
    }
}
