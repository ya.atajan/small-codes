import java.util.Arrays;
import java.util.Scanner;

public class diving {
    private static Scanner keyboard = new Scanner(System.in);
    private static float[] scores = new float[7];
    private static int arrayIndex = 0;

    private static float max(float[] array) {
        float maxValue = array[0];
        for (int i = 1; i < array.length; i++) {
            if (array[i] > maxValue) {
                maxValue = array[i];
            }
        }
        return maxValue;
    }

    private static float min(float[] array) {
        float minValue = array[0];
        for (int i = 1; i < array.length; i++) {
            if (array[i] < minValue) {
                minValue = array[i];
            }
        }
        return minValue;
    }

    private static float[] inputAllScores(float score) {
        if (arrayIndex < scores.length) {
            scores[arrayIndex] = score;
            arrayIndex++;
        }
        return scores;
    }

    private static float inputValidScore() {
        float score = keyboard.nextFloat();

        if (score < 0 && score > 10) {
            System.out.println("Not a valid score. Try again.");
            inputValidScore();
        }
        return score;
    }

    private static float inputValidDegreeOfDifficulty() {
        float difficulty = keyboard.nextFloat();

        if (difficulty < 1.2 && difficulty > 3.8) {
            System.out.println("Not a valid difficulty. Try again.");
            inputValidDegreeOfDifficulty();
        }
        return difficulty;
    }

    private static float calculateScore(float[] scores, float difficulty) {
        float min = min(scores);
        float max = max(scores);
        float total = 0;

        for (int i = 0; i < 7; i++) {
            total += scores[i];
        }

        return (float) (((total - min - max) * difficulty) * 0.6);
    }

    public static void main(String[] args) {
        for (int i = 0; i < 7; i++) {
            System.out.println("Enter score");
            float score = inputValidScore();
            inputAllScores(score);
        }
        System.out.println(Arrays.toString(scores));
        System.out.println("Enter difficulty");
        float difficulty = inputValidDegreeOfDifficulty();

        System.out.println(calculateScore(scores, difficulty));
    }
}
