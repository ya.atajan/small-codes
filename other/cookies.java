import java.util.Scanner;

public class cookies {
    private static String[] statText = new String[]{"0 to 10", "11 to 20", "21 to 30", "31 to 40", "41 or more"};
    private static int[] stat = new int[5];

    private static void check(int numCookies) {
        if (numCookies <= 10) {
            stat[0] += 1;
        } else if (numCookies <= 20) {
            stat[1] += 1;
        } else if (numCookies <= 30) {
            stat[2] += 1;
        } else if (numCookies <= 40) {
            stat[3] += 1;
        } else {
            stat[4] += 1;
        }
    }

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);

        System.out.print("Total number of girls: ");
        int girls = keyboard.nextInt();

        for(int i = 0; i < girls; i++) {
            String console = String.format("Cookies sold by girl %d: ", i + 1);

            System.out.println(console);
            int cookies = keyboard.nextInt();

            check(cookies);
        }

        String space = "         ";
        System.out.println("Total boxes" + space + "Number of girl scouts");
        for (int i = 0; i < stat.length; i++) {
            System.out.println(statText[i] + space + stat[i]);
        }
    }
}