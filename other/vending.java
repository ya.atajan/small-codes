import java.util.Scanner;

public class vending {
    private static String[] itemsArray = {"apple", "beer", "chips", "cookies", "soda"};
    private static double[] pricesArray = {2.0, 3.0, 4.0, 1.21 ,0.5};
    private static double money = 0.0;
    private static int actionPoint = 4;

    private static boolean check(int choice) {
        int itemIndex = choice - 1;
        return money >= pricesArray[itemIndex];
    }

    private static int[] change() {
        int int_money = (int)(money * 100);
        int toDime = int_money % 25;
        int rQ = int_money / 25;
        int toN = toDime % 10;
        int rD = toDime / 10;
        int toP = toN % 5;
        int rN = toN / 5;

        return new int[]{rQ, rD, rN, toP};
    }

    public static void main(String[] args) {
        for (int i = 0; i < 3; i++) {
            Scanner keyboard = new Scanner(System.in);

            System.out.println("enter money");
            double myDouble = keyboard.nextDouble();

            if (myDouble == 1 || myDouble == 5) {
                money += myDouble;

                while (actionPoint > 0) {
                    System.out.println("enter item choice");
                    int choice = keyboard.nextInt();
    
                    if (check(choice)) {
                        money -= pricesArray[choice - 1];
                        System.out.println("You got: " + itemsArray[choice-1]);
                        System.out.println("Q: " + change()[0] + " D: " + change()[1] + " N: " + change()[2] + " P: " + change()[3]);
                        break;
                    } else {
                        System.out.println("you broke");
                        actionPoint--;
                    }
                }
            } else {
                System.out.println("Money most be 1 or 5");
            }
            money = 0.0;
        }
    }
}