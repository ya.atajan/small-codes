import java.util.Arrays;
import java.util.Scanner;

public class direction {

    private static String getDir(String[] splitted) {
        int[] p0 = {Integer.valueOf(splitted[0]), Integer.valueOf(splitted[1])};
        int[] p1 = {Integer.valueOf(splitted[2]), Integer.valueOf(splitted[3])};
        int[] p2 = {Integer.valueOf(splitted[4]), Integer.valueOf(splitted[5])};

        int dir = ((p1[0] - p0[0])*(p2[1]-p0[1])-(p2[0]-p0[0])*(p1[1]-p0[0]));

        if (dir > 0) {
            return "left";
        } else if (dir == 0) {
            return "same";
        } else {
            return "right";
        }
    }

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        System.out.println("enter 6 coordinate int separated by space. Format is p0 ~ p2");
        String coordinates = keyboard.nextLine();
        String[] splitted = coordinates.split("\\s+");
        System.out.println(Arrays.toString(splitted));
        System.out.println(getDir(splitted));
    }
}
