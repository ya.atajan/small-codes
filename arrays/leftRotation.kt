package arrays

import java.util.*


fun main(args: Array<String>) {
    val scan = Scanner(System.`in`)
    val nd = scan.nextLine().split(" ")
    val n = nd[0].trim().toInt()
    val d = nd[1].trim().toInt()
    val arr: Array<Int> = Array(n){0}

    if (n == 0 || d == 0 || n == d) {
        for (i in 0 until n) {
            arr[i] = i + 1
        }
    } else {
        for (i in 0 until d) {
            arr[i] = i + 1
        }

        for (i in d downTo 1) {
            arr[i] = d - i
        }
    }

    for (i in arr) {
        System.out.print(arr[i])
        System.out.print(" ")
    }
}