package arrays

import java.util.*

// Complete the arrays.jumpingOnClouds function below.
fun jumpingOnClouds(c: Array<Int>): Int {
    var jump = 0
    var index = 0

    while (index < c.size - 1) {
        if (index +2 <= c.size-1 && c[index+2] == 0) {
            jump ++
            index += 2
        } else {
            jump ++
            index ++
        }
    }
    return jump
}

fun main(args: Array<String>) {
    val scan = Scanner(System.`in`)

    val n = scan.nextLine().trim().toInt()

    val c = scan.nextLine().split(" ").map{ it.trim().toInt() }.toTypedArray()

    val result = jumpingOnClouds(c)

    println(result)
}
