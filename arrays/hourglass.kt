package arrays

import java.util.*

// Complete the arrays.hourglassSum function below.
fun hourglassSum(arr: Array<Array<Int>>): Int {
    val height = arr.size
    val width = arr[0].size
    var max = Integer.MIN_VALUE

    for (y in 0 until height - 2) {
        for (x in 0 until width - 2) {
            val total = top(arr[y], x) + middle(arr[y + 1], x) + bottom(arr[y + 2], x)
            if (total > max) {
                max = total
            }
        }
    }
    return max
}

fun top(arr: Array<Int>, index: Int): Int {
    return arr[index] + arr[index + 1] + arr[index + 2]
}

fun middle(arr: Array<Int>, index: Int): Int {
    return arr[index + 1]
}

fun bottom(arr: Array<Int>, index: Int): Int {
    return arr[index] + arr[index + 1] + arr[index + 2]
}

fun main(args: Array<String>) {
    val scan = Scanner(System.`in`)

    val arr = Array<Array<Int>>(6, { Array<Int>(6, { 0 }) })

    for (i in 0 until 6) {
        arr[i] = scan.nextLine().split(" ").map { it.trim().toInt() }.toTypedArray()
    }

    val result = hourglassSum(arr)

    println(result)
}
