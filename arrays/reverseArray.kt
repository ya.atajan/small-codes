package arrays

import java.util.*

// Complete the arrays.reverseArray function below.
fun reverseArray(a: Array<Int>): Array<Int> {
    val length = a.size
    val newAr = Array(length) { 0 }

    for (i in 0 until length) {
        newAr[i] = a[length - (i + 1)]
    }
    return newAr
}

fun main(args: Array<String>) {
    val scan = Scanner(System.`in`)

    val arrCount = scan.nextLine().trim().toInt()

    val arr = scan.nextLine().split(" ").map { it.trim().toInt() }.toTypedArray()

    val res = reverseArray(arr)

    println(res.joinToString(" "))
}
