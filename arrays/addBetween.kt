package arrays

import java.util.*

fun main(args: Array<String>) {
    val scan = Scanner(System.`in`)
    val N = scan.nextInt()
    var M = scan.nextInt()

    /* Save interval endpoint's "k" values in array */
    val array = LongArray(N + 1)
    while (M-- > 0) {
        val a = scan.nextInt()
        val b = scan.nextInt()
        val k = scan.nextInt()
        array[a - 1] += k.toLong()
        array[b] -= k.toLong()
    }
    scan.close()

    /* Find max value */
    var sum: Long = 0
    var max: Long = 0
    for (i in 0 until N) {
        sum += array[i]
        max = Math.max(max, sum)
    }

    println(max)
}