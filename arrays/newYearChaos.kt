package arrays

import java.util.*

// Complete the arrays.minimumBribes function below.
fun minimumBribes(q: Array<Int>): String {
    val length = q.size
    var count = 0

    for (i in 0 until length) {
        if (q[i] > i + 1) {
            val change = Math.abs(q[i] - (i + 1))

            if (change < 3) {
                count += change
            } else {
                return "Too chaotic"
            }
        }
    }
    return count.toString()
}

fun checkChaos(q: Array<Int>): Boolean {
    for (i in 0 until q.size) {
        if (q[i] > i + 2) {
            System.out.print("Too chaotic")
            return true
        }
    }
    return false
}

fun main(args: Array<String>) {
    val scan = Scanner(System.`in`)

    val t = scan.nextLine().trim().toInt()

    for (tItr in 1..t) {
        val n = scan.nextLine().trim().toInt()

        val q = scan.nextLine().split(" ").map{ it.trim().toInt() }.toTypedArray()

        if (!checkChaos(q)) {

        }
    }
}
