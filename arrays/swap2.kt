package arrays

import java.util.*

fun minimumSwaps(arr: Array<Int>, n: Int): Int {

    var i = 0
    var c = 0
    while (i < n) {

        if (arr[i] == i + 1) {
            i++
            continue
        }

        arr[i] = arr[arr[i]-1].also { arr[arr[i]-1] = arr[i] }
        c++
    }
    return c
}

fun main(args: Array<String>) {
    val scan = Scanner(System.`in`)
    val n = scan.nextLine().trim().toInt()
    val arr = scan.nextLine().split(" ").map{ it.trim().toInt() }.toTypedArray()
    System.out.print(minimumSwaps(arr, n))
}