package dict_and_hash

import java.util.*

// Complete the checkMagazine function below.
fun checkMagazine(magazine: Array<String>, note: Array<String>): String {
    val context = HashMap<String, Int>()

    for (word in magazine) {
        if (!context.containsKey(word)) {
            context[word] = 1
        } else {
            context[word] = context[word]!! + 1
        }
    }

    for (word in note) {
        if (context.containsKey(word) && context[word]!! > 0) {
            context[word] = context[word]!! - 1
        } else {
            return "No"
        }
    }
    return "Yes"
}

fun main(args: Array<String>) {
    val scan = Scanner(System.`in`)

    val mn = scan.nextLine().split(" ")

    val m = mn[0].trim().toInt()

    val n = mn[1].trim().toInt()

    val magazine = scan.nextLine().split(" ").toTypedArray()

    val note = scan.nextLine().split(" ").toTypedArray()

    System.out.print(checkMagazine(magazine, note))
}
