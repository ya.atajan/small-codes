package dict_and_hash

import java.util.*

//TODO:DO

// Complete the sherlockAndAnagrams function below.
fun sherlockAndAnagrams(s: String): Int {
    val original = s.toCharArray()
    var increment = 0
    var start = 0
    var end = 1
    var found = 0

    while (increment < original.size) {
        while (end < original.size) {
//            System.out.println(original.slice(IntRange(0, increment)))
//            System.out.println(original.slice(IntRange(start + increment, end)))
            if (original.slice(IntRange(0, increment)) == original.slice(IntRange(start + increment, end))) {
                found++
            }
            end++
            start++
        }
        increment++
    }

    return found
}

fun main(args: Array<String>) {
    val scan = Scanner(System.`in`)

    val q = scan.nextLine().trim().toInt()

    for (qItr in 1..q) {
        val s = scan.nextLine()

        val result = sherlockAndAnagrams(s)

        println(result)
    }
}
