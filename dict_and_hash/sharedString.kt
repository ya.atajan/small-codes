package dict_and_hash

import java.util.*
import kotlin.collections.HashSet

fun twoStrings(s1: String, s2: String): String {
    val letters = HashSet<Char>()

    for (letter in s1) {
        letters.add(letter)
    }
    for (letter in s2) {
        if (letters.contains(letter)) {
            return "YES"
        }
    }
    return "NO"
}

fun main(args: Array<String>) {
    val scan = Scanner(System.`in`)

    val q = scan.nextLine().trim().toInt()

    for (qItr in 1..q) {
        val s1 = scan.nextLine()

        val s2 = scan.nextLine()

        val result = twoStrings(s1, s2)

        println(result)
    }
}